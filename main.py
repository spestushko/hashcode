from hashcode2018.ridehailing import Solver as RideHailingSolver


def solve_ride_hailing(algo, fp):
    solver = RideHailingSolver(algo, fp)
    solver.solve()


if __name__ == '__main__':
    solve_ride_hailing('greedy', 'A')
