from .City import City
from .Ride import Ride
from .Car import Car


class DatasetParser:

    @staticmethod
    def load(fp):
        with open(fp) as dataset:
            rides = []
            fleet = []
            rows, columns, vehicles_count, rides_count, bonus, steps = [int(x) for x in dataset.readline().split()]
            for i in range(rides_count):
                a, b, x, y, s, f = [int(x) for x in dataset.readline().split()]
                rides.append(Ride(i, (a, b), (x, y), s, f))
            for i in range(vehicles_count):
                fleet.append(Car(i, (0, 0)))
            return City((rows, columns), fleet, rides, bonus, steps)



