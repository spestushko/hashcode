import numpy as np


class Ride:
    def __init__(self, uid, start_pos, end_pos, start_t, end_t):
        self.id = uid
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.start_t = start_t
        self.end_t = end_t

        self._s_row, self._s_col = self.start_pos
        self._e_row, self._e_col = self.end_pos

    def calc_distance(self):
        return sum([abs(v) for v in np.subtract(self.start_pos, self.end_pos)])

    def __lt__(self, other):
        self_id = self.id
        other_id = other.id
        return self_id < other_id

    def __str__(self):
        return 'Ride between: {} - {}, [{}] -> [{}]'.format(self.start_t, self.end_t, self.start_pos, self.end_pos)
