import numpy as np
from queue import PriorityQueue


class Greedy:
    def __init__(self, city):
        self.city = city
        self.priority = PriorityQueue()
        self.in_progress = PriorityQueue()

    def _rides_at(self, t):
        return self.city.get_rides_at(t) or []

    def _prioritize(self, rides):
        for ride in rides:
            self.priority.put((ride.end_t - ride.start_t, ride))

    def _process_per_priority(self):
        updated_priority = PriorityQueue()
        while not self.priority.empty():
            ride_found = False
            urgency, ride = self.priority.get()
            ride_distance = ride.calc_distance()
            for car in self.city.fleet:
                near_car_distance = Greedy._car_distance(ride, car)
                if near_car_distance + ride_distance <= urgency and car.is_available():
                    bonus = urgency - near_car_distance - ride_distance
                    car.flip_availability()
                    self.in_progress.put((near_car_distance + ride_distance, (ride, car, bonus)))
                    print('Scheduled ride with duration={} for car={}, '.format(bonus, car.id))
                    ride_found = True
                    break
            if not ride_found and urgency > 0:
                updated_priority.put((urgency - 1, ride))
        self.priority = updated_priority

    def _update_in_progress_rides(self):
        updated_in_progress = PriorityQueue()
        while not self.in_progress.empty():
            status, trip = self.in_progress.get()
            ride, car, bonus = trip
            status -= 1
            if status <= 0:
                car.add_trip((ride, bonus))
                car.pos = ride.end_pos
                car.flip_availability()
                print('Car with id={} is available'.format(car.id))
            else:
                updated_in_progress.put((status, trip))
        self.in_progress = updated_in_progress

    def _find_driver(self, ride, cars):
        pass

    @staticmethod
    def _car_distance(ride, car):
        return sum([abs(v) for v in np.subtract(ride.start_pos, car.pos)])

    def run(self):
        t = -1
        while t < self.city.steps:
            # Increment step count
            t += 1
            print('\nSim at step', t)
            rides = self._rides_at(t)
            for ride in rides:
                print(ride)
            self._prioritize(rides)
            self._process_per_priority()
            self._update_in_progress_rides()
        return self.city.dump_fleet_report()
        # Todo: dump results into a file
