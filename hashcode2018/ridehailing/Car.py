class Car:
    def __init__(self, uid, pos):
        self.id = uid
        self.pos = pos

        self._trips = []
        self._avail = True

    def is_available(self):
        return self._avail

    def flip_availability(self):
        self._avail = not self._avail

    def add_trip(self, ride):
        self._trips.append(ride)

    def report(self):
        report = str(len(self._trips))
        print('Report, len trips: {}'.format(len(self._trips)))
        for trip in self._trips:
            ride, bonus = trip
            report += ' {}'.format(ride.id)
        return report
