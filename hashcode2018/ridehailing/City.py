class City:
    def __init__(self, size, fleet, rides, bonus, steps):
        self.size = size
        self.fleet = fleet
        self.rides = rides
        self.steps = steps
        self.bonus = bonus

        self._rows, self._cols = self.size
        self._schedule = {}

        self._init_schedule()

    def _init_schedule(self):
        for ride in self.rides:
            if ride.start_t not in self._schedule:
                self._schedule[ride.start_t] = []
            self._schedule[ride.start_t].append(ride)

    def get_rides_at(self, step):
        if step in self._schedule:
            return self._schedule[step]
        return None

    def dump_fleet_report(self):
        report = ''
        for car in self.fleet:
            report += car.report() + '\n'
        return report

    def __str__(self):
        return 'City size: {}\n' \
               'Fleet size: {}\n' \
               'Total rides: {}\n' \
               'Bonus per ride: {}\n' \
               'Simulation steps: {}'.format(self.size, len(self.fleet), len(self.rides), self.bonus, self.steps)
