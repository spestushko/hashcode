from .DatasetParser import DatasetParser
from .algorithms.Greedy import Greedy

import os
from enum import Enum


class Solver:

    class Datasets(Enum):
        A = 'a_example.in'
        B = 'b_should_be_easy.in'
        C = 'c_no_hurry.in'
        D = 'd_metropolis.in'
        E = 'e_high_bonus.in'

    class Algorithms(Enum):
        GREEDY = Greedy

    def __init__(self, algorithm, dataset):
        self.algorithm = algorithm.upper()
        self.dataset = dataset.upper()

    def solve(self):
        path = os.path.dirname(os.path.realpath(__file__))
        city = DatasetParser.load(os.path.join(path, './datasets/{}'.format(Solver.Datasets[self.dataset].value)))
        algo = Solver.Algorithms[self.algorithm].value(city)
        print(city, '\n')
        result = algo.run()
        print(result)



